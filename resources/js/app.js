require('./bootstrap');

window.Vue = require('vue')

Vue.component('category-main-component', require('./categories/components/CategoryMain.vue').default)

new Vue({
	el: "#app"
})